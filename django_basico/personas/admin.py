# -*- coding: utf-8 -*-
#Esta línea de arriba, habilita caracteres con acentos, caracteres especiales, etc.
#################################################################################
# Autor: Axel Díaz <diaz.axelio@gmail.com>                                      #
# Aplicación: Personas                                                          #
# Fecha: 9/12/2011                                                              #
#################################################################################
from personas.models import * # Importamos todas las clases que hayan en personas
from django.contrib import admin
#from django import forms

class PersonasAdmin(admin.ModelAdmin):
    search_fields=['cedula','nombre','apellido'] # Hacemos que haya un campo de búsqueda, y que busque por cedula, nombre o apellido
admin.site.register(Personas,PersonasAdmin) #Definimos que la clase Personas va a ser administrado por PersonasAdmin
