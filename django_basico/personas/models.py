# -*- coding: utf-8 -*-
#Esta línea de arriba, habilita caracteres con acentos, caracteres especiales, etc.
#################################################################################
# Autor: Axel Díaz <diaz.axelio@gmail.com>                                      #
# Aplicación: Personas                                                          #
# Fecha: 9/12/2011                                                              #
#################################################################################
from django.db import models

# Create your models here.
class Personas(models.Model):
    #Damos un ejemplo de cada tipo de dato o al menos los más utilizados
    #Cédula
    cedula = models.IntegerField() #Cédula de tipo Entero

    #Nombre
    nombre = models.CharField(max_length=30) #Nombre de tipo Caracter, obligatoriamente hay que declararle la longitud máxima permitida

    #Apellido
    apellido = models.CharField(max_length=30) #Nombre de tipo Caracter, obligatoriamente hay que declararle la longitud máxima permitida

    #Fecha de nacimiento
    fecha_nac = models.DateField() #Fecha de nacimiento de tipo Fecha

    #Estatura
    estatura = models.FloatField() #Estatura de tipo flotante para decimales

    #Estado Civil
    edo_civil = models.IntegerField(choices=((0,'Casado(a)'),(1,'Soltero(a)'),(2,'Divorciado(a)'),(3,'Viudo(a)')), default=0) #Establecemos 4 opciones y especificamos que por defecto empiece por 0. Entero porque guardará el número asociado, es decir 0, 1, 2 ó 3

    #¿Mayor de edad?
    mayor_edad = models.BooleanField(default=False) #Mayoría de edad de tipo booleano

    class Meta:
        db_table = u'personas' #Especificamos el nombre que llevará esta tabla en la base de datos, la u es para especificar que acepte unicodes (acentos, caracteres especiales, etc)
        verbose_name_plural = u'personas' #Especificamos qué nombre llevará la tabla en caso que necesite nombrar la tabla en plural, si no lo hacemos agregará una S al nombre de la tabla por defecto y quedaría "personass"
    def __unicode__(self):
        return u'%s - %s %s' %(self.cedula, self.nombre, self.apellido) #Si hacemos una llave foránea desde otra tabla, los datos de una persona aparecerán en primera instancia como "cédula - nombre apellido"
